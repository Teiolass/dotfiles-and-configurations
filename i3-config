# set the font
font pango: Roboto Mono 10

force_focus_wrapping yes

# useful variable to set the wallpaper
set $lock_paper /home/teiolass/Pictures/Wallpapers/gris_hand.png
set $wallpaper /home/teiolass/Pictures/xkcd_recolored/death_hand-recolored.png


# Common key variables
set $up k
set $down j
set $left h
set $right l
set $mod Mod4

# Useful shortcuts for common programs
bindsym $mod+g exec firefox
bindsym $mod+m exec zathura
bindsym $mod+y exec "LD_PRELOAD=/usr/lib/spotify-adblock.so spotify"
bindsym $mod+Return exec kitty
bindsym $mod+d exec "/home/teiolass/.scripts/i3-dmenu.sh"

# Multi screen things
bindsym $mod+u move workspace to output next
bindsym $mod+Shift+u move workspace to output primary
bindsym $mod+period focus output down
bindsym $mod+Shift+period focus output primary

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+q kill

############################
####                    ####
####  MOVEMENT THINGS   ####
####                    ####
############################

# change focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+comma focus next

bindsym $mod+n fullscreen toggle; focus right; fullscreen toggle

# split in horizontal orientation
bindsym $mod+c split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

bindsym $mod+o sticky toggle 



# THIS SHOULD WORK WITH EVERY APP
#bindsym XF86AudioPlay exec playerctl play-pause
#bindsym XF86AudioPause exec playerctl play-pause
#bindsym XF86AudioNext exec playerctl next
#bindsym XF86AudioPrev exec playerctl previous

# THIS IS FOR SPOTIFY ONLY
#bindsym XF86AudioPlay exec playerctl -p spotify play-pause
#bindsym XF86AudioPause exec playerctl -p spotify play-pause
#bindsym XF86AudioNext exec playerctl -p spotify next
#bindsym XF86AudioPrev exec playerctl -p spotify previous

# THIS IS FOR EVERY APP, PRIORITIZING SPOTIFY
bindsym XF86AudioPlay exec "playerctl --player=spotify,%any play-pause"
bindsym XF86AudioPause exec "playerctl --player=spotify,%any play-pause"
bindsym XF86AudioNext exec "playerctl --player=spotify,%any next"
bindsym XF86AudioPrev exec "playerctl --player=spotify,%any previous"

# dunst controller
bindsym $mod+slash exec dunstctl close
bindsym $mod+Shift+slash exec dunstctl close-all
bindsym $mod+grave exec dunstctl history-pop


#######################
####               ####
####   WORKSPACES  ####
####               ####
#######################

set $wk0 "10"
set $wk1 "1"
set $wk2 "2"
set $wk3 "3"
set $wk4 "4"
set $wk5 "5"
set $wk6 "6"
set $wk7 "7"
set $wk8 "8"
set $wk9 "9"

# switch to workspace
bindsym $mod+0 workspace $wk0
bindsym $mod+1 workspace $wk1
bindsym $mod+2 workspace $wk2
bindsym $mod+3 workspace $wk3
bindsym $mod+4 workspace $wk4
bindsym $mod+5 workspace $wk5
bindsym $mod+6 workspace $wk6
bindsym $mod+7 workspace $wk7
bindsym $mod+8 workspace $wk8
bindsym $mod+9 workspace $wk9

bindsym $mod+Tab       workspace next_on_output
bindsym $mod+Shift+Tab workspace prev_on_output
bindsym $mod+i         workspace back_and_forth

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $wk1
bindsym $mod+Shift+2 move container to workspace $wk2
bindsym $mod+Shift+3 move container to workspace $wk3
bindsym $mod+Shift+4 move container to workspace $wk4
bindsym $mod+Shift+5 move container to workspace $wk5
bindsym $mod+Shift+6 move container to workspace $wk6
bindsym $mod+Shift+7 move container to workspace $wk7
bindsym $mod+Shift+8 move container to workspace $wk8
bindsym $mod+Shift+9 move container to workspace $wk9
bindsym $mod+Shift+0 move container to workspace $wk0


# SYSTEM THINGS

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"


# RESIZE MODE

# resize window (you can also use the mouse for that)
mode "resize" {
        # Pressing down will grow the window’s height.
        bindsym $left       resize shrink width  30 px or 30 ppt
        bindsym $down       resize grow   height 30 px or 30 ppt
        bindsym $up         resize shrink height 30 px or 30 ppt
        bindsym $right      resize grow   width  30 px or 30 ppt

        # same bindings, but for the arrow keys
        bindsym Left        resize shrink width  30 px or 30 ppt
        bindsym Down        resize grow   height 30 px or 30 ppt
        bindsym Up          resize shrink height 30 px or 30 ppt
        bindsym Right       resize grow   width  30 px or 30 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"



##
## COLORS
##


# set primary gruvbox colorscheme colors
#
set $bg #282828
set $red #cc241d
set $green #98971a
set $yellow #d79921
set $blue #458588
set $purple #b16286
set $aqua #689d68
set $gray #a89984
set $darkgray #1d2021


# green gruvbox
# class                 border|backgr|text|indicator|child_border
client.focused          $yellow $yellow $darkgray $aqua $aqua
client.focused_inactive $yellow $yellow $darkgray $yellow $yellow
client.unfocused        $darkgray $darkgray $yellow $yellow $yellow
client.urgent           $red $red $darkgray $red $red

### POLYBAR
exec_always --no-startup-id $HOME/.config/polybar/start.sh

### REDSHITFT
exec_always --no-startup-id "redshift -P"

# Set i3lock
bindsym $mod+p exec --no-startup-id  "/home/teiolass/.scripts/lock2 $lock_paper"
bindsym $mod+shift+p exec "sleep 1 && xset dpms force off"
exec "/home/teiolass/.scripts/lock2 $lock_paper"


#time sync
exec ntpd -qg

# remap keys
exec xmodmap "/home/teiolass/.Xmodmap"


# ACTION KEYS
# Just don't ask


# Pulse Audio controls
# bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5% #increase sound volume
# bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume
# bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer -D pulse sset Master 5%+
bindsym XF86AudioLowerVolume exec --no-startup-id amixer -D pulse sset Master 5%-
bindsym XF86AudioMute exec --no-startup-id amixer -D pulse set Master 1+ toggle

# These ones require light package
# Warning: it needs the sudo previlegies or the udev rules + user in group video
# Read the readme of light for the whole infos
# bindsym XF86MonBrightnessUp exec light -A 10
# bindsym XF86MonBrightnessDown exec light -U 10
bindsym XF86MonBrightnessUp exec /home/teiolass/.scripts/bright 1.0
bindsym XF86MonBrightnessDown exec /home/teiolass/.scripts/bright 0.55


# Strange things that make system work & bg
exec_always feh --bg-scale $wallpaper
# exec_always xrdb ~/.Xresources
exec_always export PATH=${PATH):${HOME}/.scripts
exec --no-startup-id dunst -config /home/teiolass/.config/dunst/dunstrc
bindsym Print exec scrot -o /home/teiolass/tmp/screen.png

# To find the class: use xprop and click on the window you want
for_window [class="Spotify"] move to workspace $wk0, focus, gaps outer current set 0, gaps inner current set 0, border pixel 0
for_window [class="TelegramDesktop"] move to workspace $wk5, focus, gaps outer current set 0, gaps inner current set 0, border pixel 0
for_window [class="Zotero"] move to workspace $wk9, focus, gaps outer current set 0, gaps inner current set 0, border pixel 0
for_window [class="obsidian"] move to workspace $wk8, focus, border pixel 0, fullscreen toggle
for_window [class="Anki"] move to workspace $wk7, focus, gaps outer current set 0, gaps inner current set 0, border pixel 0

for_window [class="floating_window"] floating enable

# Border things
# new_window pixel 0
default_border          pixel 0
default_floating_border pixel 4
for_window [class="^.*"] border pixel 0
for_window [floating] border pixel 4
# used to hide the window borders on the margin of the screen
hide_edge_borders smart

# GAAAAPS

# smart_gaps on
# smart_borders on
set $mode_gaps Gaps: (o) outer, (i) inner, (n) normal, (a) default, (0) compact, (b) remove borders
set $mode_gaps_outer Outer Gaps: Use l/k (Shift for global)
set $mode_gaps_inner Inner Gaps: Use l/k (Shift for global)
set $mode_gaps_normal Normal mode a/0
bindsym $mod+b mode "$mode_gaps"
bindsym $mod+shift+b exec "i3-msg gaps inner current set 0 && i3-msg gaps outer current set 0 && i3-msg border pixel 0"
gaps inner 0
gaps outer 0


mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym n      mode "$mode_gaps_normal"
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym a exec "i3-msg gaps inner current set 20 && i3-msg gaps outer current set 30 && i3-msg border pixel 4"
        bindsym 0 exec "i3-msg gaps inner current set 0 && i3-msg gaps outer current set 0 && i3-msg border pixel 0"
        bindsym b exec "i3-msg border pixel 4"
}

mode "$mode_gaps_normal" {
        bindsym a exec "i3-msg gaps inner current set 20 && i3-msg gaps outer current set 30 && i3-msg border pixel 4"
        bindsym 0 exec "i3-msg gaps inner current set 0 && i3-msg gaps outer current set 0 && i3-msg border pixel 0"

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
        bindsym l  gaps inner current plus 5
        bindsym k gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+l  gaps inner all plus 5
        bindsym Shift+k gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym l  gaps outer current plus 5
        bindsym k gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+l  gaps outer all plus 5
        bindsym Shift+k gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
