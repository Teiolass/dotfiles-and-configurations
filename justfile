load:
    echo "This command is incomplete!!!"

clean:
    rm dunstrc
    rm i3-config
    rm kitty.conf
    rm -rf nvim
    rm polybar-config
    rm -rf rofi
    rm -rf scripts
    rm -rf zathurarc
    rm -rf zshrc

nvim:
    rm -rf nvim
    mkdir nvim
    cp -r ~/.config/nvim/* ./nvim
    rm -rf nvim/plugged
