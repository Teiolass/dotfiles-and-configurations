syn match mdTag "\v#[a-zA-Z0-9_\-]+"
syn region mdLink start="\[\[" end="\]\]" oneline
syn region mdMath start="\v\s\$" end="\$" oneline
syn region mdMath start="\v\s\$\$" end="\$\$" oneline

highlight def link  mdMath markdownCode
