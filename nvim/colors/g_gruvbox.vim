hi clear
if exists("syntax_on")
  syntax reset
endif

let g:gruvbox_contrast_dark = 'hard'
runtime colors/gruvbox_original.vim

let g:colors_name='gruvbox'

let s:gb = {}

" fill it with absolute colors
let s:gb.dark0_hard  = '#1d2021'
let s:gb.dark0       = '#282828'
let s:gb.dark0_soft  = '#32302f'
let s:gb.dark1       = '#3c3836'
let s:gb.dark2       = '#504945'
let s:gb.dark3       = '#665c54'
let s:gb.dark4       = '#7c6f64'
let s:gb.dark4_256   = '#7c6f64'

let s:gb.gray_245    = '#928374'
let s:gb.gray_244    = '#928374'

let s:gb.light0_hard = '#f9f5d7'
let s:gb.light0      = '#fbf1c7'
let s:gb.light0_soft = '#f2e5bc'
let s:gb.light1      = '#ebdbb2'
let s:gb.light2      = '#d5c4a1'
let s:gb.light3      = '#bdae93'
let s:gb.light4      = '#a89984'
let s:gb.light4_256  = '#a89984'

let s:gb.bright_red     = '#fb4934'
let s:gb.bright_green   = '#b8bb26'
let s:gb.bright_yellow  = '#fabd2f'
let s:gb.bright_blue    = '#83a598'
let s:gb.bright_purple  = '#d3869b'
let s:gb.bright_aqua    = '#8ec07c'
let s:gb.bright_orange  = '#fe8019'

let s:gb.neutral_red    = '#cc241d'
let s:gb.neutral_green  = '#98971a'
let s:gb.neutral_yellow = '#d79921'
let s:gb.neutral_blue   = '#458588'
let s:gb.neutral_purple = '#b16286'
let s:gb.neutral_aqua   = '#689d6a'
let s:gb.neutral_orange = '#d65d0e'

let s:gb.faded_red      = '#9d0006'
let s:gb.faded_green    = '#79740e'
let s:gb.faded_yellow   = '#b57614'
let s:gb.faded_blue     = '#076678'
let s:gb.faded_purple   = '#8f3f71'
let s:gb.faded_aqua     = '#427b58'
let s:gb.faded_orange   = '#af3a03'

let s:bg = s:gb.dark0_hard
let s:fg = s:gb.light0
let s:grey   = s:gb.gray_245
let s:red    = s:gb.bright_red 
let s:green  = s:gb.bright_green 
let s:yellow = s:gb.bright_yellow 
let s:blue   = s:gb.bright_blue 
let s:purple = s:gb.bright_purple 
let s:aqua   = s:gb.bright_aqua 
let s:orange = s:gb.bright_orange 

let s:none = 'NONE'

function! s:HL(group, fg, ...)
  let mfg = a:fg
  if a:0 >= 1
    let mbg = a:1
  else
    let mbg = s:none
  endif
  if a:0 >= 2 && strlen(a:2)
    let gui = a:2
  else
    let gui = 'NONE,'
  endif

  let histring = [ 'hi', a:group,
        \ 'guifg=' . mfg,
        \ 'guibg=' . mbg,
        \ 'gui=' . gui,
        \ ]
  execute join(histring, ' ')
endfunction

call s:HL('Undercurl', s:fg, s:bg, 'undercurl')
call s:HL('Underdouble', s:fg, s:bg, 'underdouble')
call s:HL('Underdashed', s:fg, s:bg, 'underdashed')
call s:HL('Standout', s:fg, s:bg, 'standout')
call s:HL('Italic', s:fg, s:bg, 'italic')

" See https://www.reddit.com/r/vim/comments/gs3a8d/what_is_the_highlight_group_on_the_left_of_the/
call s:HL('SignColumn', s:fg, s:bg)

call s:HL('IncSearch', s:gb.light0_hard, s:gb.dark2, 'bold')
call s:HL('Search', '#b2b527', '#333e34', 'bold')
call s:HL('Visual', s:none, s:gb.dark1)
call s:HL('NonText', s:gb.dark1)

call s:HL('SpecialKey', s:orange)

call s:HL('Number', s:purple)
call s:HL('Boolean', s:purple)
call s:HL('Float', s:purple)
call s:HL('Structure', s:yellow)
call s:HL('String', s:blue)
call s:HL('Function', s:green)
call s:HL('FunctionDefinition', s:green, s:none, 'bold')
call s:HL('IdentifierImportant', s:fg, s:none, 'underline')
call s:HL('TypeImportant', s:yellow, s:none, 'bold')
call s:HL('VariableBuiltin', s:none, s:none, 'bold')
call s:HL('Todo', s:fg, s:none, 'bold,underline')
call s:HL('Identifier', s:fg)
call s:HL('Statement', s:red)
call s:HL('StorageClass', s:red)
call s:HL('Type', s:yellow)
call s:HL('PreProc', s:aqua)
call s:HL('Comment', s:grey)
call s:HL('Constant', s:purple)
call s:HL('Special', s:fg)
call s:HL('Operator', s:orange, s:none, 'bold')

call s:HL('@constructor', s:yellow, s:none, s:none)
call s:HL('@constant.builtin', s:purple)
call s:HL('@variable.builtin', s:purple)
call s:HL('@function.builtin', s:green)
call s:HL('@function.call', s:green)
call s:HL('@function', s:green, s:none, 'bold')
call s:HL('@type.qualifier', s:red)
call s:HL('@attribute', s:red)
call s:HL('@string.escape', s:orange)

call s:HL('@function.builtin.zig', s:aqua)
call s:HL('@label.zig', s:aqua)

" for latex
call s:HL('@text.environment', s:red)
call s:HL('@text.environment.name', s:fg)
call s:HL('@namespace', s:red)

call s:HL('TelescopeSelection', s:yellow, s:none, 'bold')

call s:HL('BookmarkSign',           s:green , s:none, 'bold')
call s:HL('BookmarkAnnotationSign', s:green , s:none, 'bold')
call s:HL('BookmarkLine',           s:green , s:none, 'bold')
call s:HL('BookmarkAnnotationLine', s:green , s:none, 'bold')

call s:HL('mdLink', s:yellow , s:none)
call s:HL('mdTag', s:aqua , s:none)
call s:HL('markdownCode', s:blue , s:none)
call s:HL('markdownCodeDelimiter', s:blue , s:none)
call s:HL('markdownCodeBlock', s:blue , s:none)
call s:HL('markdownListMarker', s:orange , s:none, 'bold')

call s:HL('@include.python', s:red , s:none, s:none)
call s:HL('@attribute.python', s:aqua)
call s:HL('@attribute.builtin.python', s:aqua)

call s:HL('debugPC', s:bg, s:green, s:none)
call s:HL('debugBreakpoint', s:bg, s:green, 'bold')

" call s:HL('FloatBorder', s:fg, s:none, s:none)
" call s:HL('Pmenu', s:fg, s:bg, s:none)
call s:HL('NormalFloat', s:fg, s:bg, s:none)
call s:HL('FloatBorder', s:fg, s:bg, s:none)

" This is for lean!!
highlight link LeanDeclarationName FunctionDefinition

set background=dark
