local builtin = require('telescope.builtin')

local definition_func = function(opts)
    local word = vim.fn.expand('<cword>')
    word = '^\\s*(using\\s*)?' .. word .. '\\s*:'
    local current_dir = vim.fn.getcwd()
    local module_dir  = '~/tmp/jai-compiler/jai/modules/'
    local search_dirs = {current_dir, module_dir}
    builtin.grep_string({default_text='', search=word, use_regex=true, search_dirs=search_dirs})
end

vim.keymap.set('n', 'gd', definition_func, {})
