require('lean').setup{
    lsp = { on_attach = on_attach },
    mappings = false,
    infoview = {
        autoopen = true,
        width = 100,
    },

    progress_bars = {
        enable = false,
    },
}
vim.api.nvim_create_autocmd('VimResized', { callback = require('lean.infoview').reposition })

require("nvim-lightbulb").setup({
    autocmd = { enabled = true },
    sign = {
        enabled = true,
        text = "💡",
        -- Highlight group to highlight the sign column text.
        hl = "LightBulbSign",
    },
})
