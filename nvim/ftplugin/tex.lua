local builtin = require('telescope.builtin')

local section_func = function(opts)
    local query = '^\\\\(sub)*section\\{.*\\}'
    local current_dir = vim.fn.getcwd()
    -- local filename = vim.fn.expand('%')

    builtin.grep_string({default_text='', search=query, use_regex=true, search_dirs={current_dir}})
end

vim.keymap.set('n', '<Leader>s', section_func, {})

