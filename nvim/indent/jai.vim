if exists("b:did_indent")
    finish
endif
let b:did_indent = 1

setlocal nosmartindent
setlocal nolisp
setlocal autoindent

setlocal indentkeys+=,0=case
setlocal indentexpr=GetJaiIndent(v:lnum)

if exists("*GetJaiIndent")
    finish
endif

function! GetJaiIndent(lnum)
    let prev = prevnonblank(a:lnum-1)

    if prev == 0
        return 0
    endif

    let prevline = getline(prev)
    let line = getline(a:lnum)

    let ind = indent(prev)

    if prevline =~ '[({]\s*$'
        let ind += &sw
    endif

    if line =~ '^\s*[)}]'
        let ind -= &sw
    endif

    if prevline =~ '^\s*case[^;]*;\s*$'
        let ind += &sw/2
    endif

    if line =~'^\s*case.*' 
        let ind -= &sw/2
    endif

    return ind
endfunction
