if exists("b:did_indent")
    finish
endif
let b:did_indent = 1

setlocal nosmartindent
setlocal nolisp
setlocal autoindent

setlocal indentexpr=GetMTexIndent(v:lnum)

if exists("*GetMTexIndent")
    finish
endif

function! GetMTexIndent(lnum)
    let prev = prevnonblank(a:lnum-1)

    if prev == 0
        return 0
    endif

    let prevline = getline(prev)
    let line     = getline(a:lnum)
    let ind      = indent(prev)

    if prevline =~ '^-'
        let  ind += &sw
    endif
    if line =~ '^-'
        let  ind -= &sw
    endif


    return ind
endfunction
