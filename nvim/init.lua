local Plug = vim.fn['plug#']
vim.call('plug#begin', '~/.config/nvim/plugged')
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
vim.cmd "Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }"
Plug 'nvim-treesitter/playground'
Plug 'kylechui/nvim-surround'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-commentary'
Plug 'mbbill/undotree'
Plug 'neovim/nvim-lspconfig'
Plug 'kaarmu/typst.vim'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-buffer'
Plug 'ggandor/leap.nvim'
Plug 'NoahTheDuke/vim-just' --this is for support on justfiles
Plug 'MattesGroeger/vim-bookmarks'
Plug 'tom-anders/telescope-vim-bookmarks.nvim'
Plug 'mickael-menu/zk-nvim'
Plug 'AckslD/nvim-neoclip.lua'
Plug 'Julian/lean.nvim'
Plug 'kosayoda/nvim-lightbulb'
Plug 'ojroques/nvim-bufdel'
Plug 'folke/zen-mode.nvim'
Plug 'nvim-lualine/lualine.nvim'
Plug 'nvim-tree/nvim-web-devicons'
Plug 'L3MON4D3/LuaSnip'
Plug 'tpope/vim-fugitive'
Plug 'stevearc/oil.nvim'
vim.call('plug#end')

vim.opt.termguicolors = true
vim.cmd [[
  silent! colorscheme g_gruvbox
]]

vim.keymap.set('n', '<Space>', '<NOP>')
vim.g.mapleader = ' '

vim.opt.tabstop        = 4
vim.opt.shiftwidth     = 4
vim.opt.expandtab      = true
vim.opt.number         = true
vim.opt.relativenumber = true
vim.opt.hlsearch       = true
vim.opt.incsearch      = true
vim.opt.splitright     = true
vim.opt.scrolloff      = 8
vim.opt.undodir        = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile       = true
vim.opt.smartindent    = true
vim.opt.updatetime     = 50
vim.opt.breakindent    = true
vim.opt.list           = true
vim.opt.listchars      = {
    leadmultispace = '⦙   ',
    tab = '; ',
    -- leadmultispace = ';   ',
    -- trail = '-'
}
vim.opt.sessionoptions = 'blank,buffers,curdir,folds,help,tabpages,winsize,terminal,globals,options,localoptions'
vim.opt.spelllang = 'en_us'

vim.g.netrw_liststyle    = 3
vim.g.netrw_browse_split = 0
vim.g.netrw_banner       = 0

vim.g.AutoPairsShortcutToggle     = ''
vim.g.AutoPairsShortcutFastWrap   = ''
vim.g.AutoPairsShortcutJump       = ''
vim.g.AutoPairsShortcutBackInsert = ''
vim.g.AutoPairsFlyMode            = 0

local actions = require('telescope.actions')
local telescope = require('telescope')
telescope.setup{
extensions = {
    fzf = {
        fuzzy = true,                    -- false will only do exact matching
        override_generic_sorter = true,  -- override the generic sorter
        override_file_sorter = true,     -- override the file sorter
        case_mode = 'smart_case',        -- or 'ignore_case' or 'respect_case' the default case_mode is 'smart_case'
    }
},
defaults = {
    mappings = {
        i = {
            ['<C-j>'] = actions.move_selection_next,
            ['<C-k>'] = actions.move_selection_previous,
            ['<C-d>'] = actions.delete_buffer
        }
    }
},
}

require('telescope').load_extension('fzf')
local builtin = require('telescope.builtin')

local telescope_sucks = function (f)
    g = function()
        vim.cmd("normal! m'")
        f()
    end
    return g
end
vim.keymap.set('n', '<leader>f', builtin.find_files, {})
vim.keymap.set('n', '<leader>F', function () 
    builtin.find_files({hidden=true, no_ignore=true, no_ignore_parent=true})
end, {})
vim.keymap.set('n', '<leader>g', telescope_sucks(builtin.live_grep), {})
vim.keymap.set('n', '<leader>G', function () 
    vim.cmd("normal! m'")
    builtin.live_grep({grep_open_files=true})
end, {})
vim.keymap.set('n', '<leader>/', telescope_sucks(builtin.current_buffer_fuzzy_find), {})
vim.keymap.set('n', '<leader>?', telescope_sucks(builtin.search_history), {})
vim.keymap.set('n', '<leader><Space>', builtin.buffers, {})
vim.keymap.set('n', '<leader>s', builtin.lsp_document_symbols, {})
vim.keymap.set('n', '<leader>S', builtin.lsp_workspace_symbols, {})
vim.keymap.set('n', '<leader>r', builtin.lsp_incoming_calls, {})
vim.keymap.set('n', '<leader>j', builtin.jumplist, {})
vim.keymap.set('n', '<leader>c', builtin.command_history, {})
vim.keymap.set('n', '<leader>`', builtin.resume, {})
vim.keymap.set('n', '<Leader>W', function() builtin.grep_string({word_match='-w'}) end, {})
vim.keymap.set('n', '<Leader>w', function() builtin.grep_string({word_match='-w', search_dirs={vim.fn.expand('%')}}) end, {})

vim.keymap.set({'n', 'v', 'x'}, '<Leader>y', '\"+y')
vim.keymap.set({'n', 'v', 'x'}, '<Leader>Y', '\"+Y')
vim.keymap.set('n', '<Leader>p', '\"+p')
vim.keymap.set('n', '<Leader>P', '\"+P')
vim.opt.clipboard = "unnamed"
vim.keymap.set({'n', 'v', 'x'}, '<A-y>', '\"*y')
vim.keymap.set({'n', 'v', 'x'}, '<A-Y>', '\"*Y')
vim.keymap.set('n', '<A-p>', '\"*p')
vim.keymap.set('n', '<A-P>', '\"*P')

vim.keymap.set('n', '<Leader>l', ':nohlsearch<CR>')

vim.keymap.set('n', '<Leader>e', vim.cmd.Ex)
vim.keymap.set('n', '<C-u>', vim.cmd.UndotreeToggle)

vim.keymap.set('x', '&',       '<Plug>(EasyAlign)')
vim.keymap.set('v', '<Enter>', '<Plug>(EasyAlign)')

local Bind_Quickfix = true;
function toggle_quickfix ()
    Bind_Quickfix = not Bind_Quickfix
    if Bind_Quickfix then
        print('Using quickfix')
    else
        print('Using location list')
    end
    list_bind()
end
function list_bind()
    if Bind_Quickfix then
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tn', ':cnext<CR>',  {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tp', ':cprev<CR>',  {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tc', ':cclose<CR>', {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>to', ':copen<CR>',  {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tf', ':crewind<CR>',{})
    else
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tn', ':lnext<CR>',  {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tp', ':lprev<CR>',  {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tc', ':lclose<CR>', {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>to', ':lopen<CR>',  {})
        vim.keymap.set({'n', 'v', 'x'}, '<leader>tf', ':lrewind<CR>',{})
    end
end
list_bind()
vim.keymap.set({'n', 'v', 'x'}, '<leader>tt', toggle_quickfix,  {})


vim.keymap.set({'n', 'v', 'x'}, 'gh', '^')
vim.keymap.set({'n', 'v', 'x'}, 'gl', '$')

vim.keymap.set({'n', 'v', 'x'}, '<A-l>', '<C-w>l')
vim.keymap.set({'n', 'v', 'x'}, '<A-h>', '<C-w>h')
vim.keymap.set({'n', 'v', 'x'}, '<A-j>', '<C-w>j')
vim.keymap.set({'n', 'v', 'x'}, '<A-k>', '<C-w>k')
vim.keymap.set({'n', 'v', 'x'}, '<A-L>', '<C-w>L')
vim.keymap.set({'n', 'v', 'x'}, '<A-H>', '<C-w>H')

vim.keymap.set({'n', 'v'}, 'j', 'gj')
vim.keymap.set({'n', 'v'}, 'k', 'gk')

vim.keymap.set('v', '<A-J>', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', '<A-K>', ":m '<-2<CR>gv=gv")

vim.keymap.set('n', '<C-c>', '<Plug>CommentaryLine j')
vim.keymap.set('v', '<C-c>', '<Plug>Commentary')

vim.keymap.set({'n', 'v', 'i'}, '<A-d>', '<C-d>zz')
vim.keymap.set({'n', 'v', 'i'}, '<A-u>', '<C-u>zz')

vim.keymap.set({'n', 'v'}, '<C-d>', '"_')

vim.keymap.set('n', 'gV', '`[v`]')
vim.keymap.set('v', '<A-v>', 'V')
vim.keymap.set('x', 'V', 'j')

vim.keymap.set({'n', 'v', 'x'}, '<A-b>', ':bnext<CR>')
vim.keymap.set({'n', 'v', 'x'}, '<A-B>', ':bprevious<CR>')
vim.keymap.set({'n', 'v', 'x'}, '<Leader>b', ':buffer#<CR>')



vim.filetype.add({
  extension = {
    typst = 'typ'
  }  
})
vim.api.nvim_create_autocmd('FileType', {
    pattern  = { 'typst' },
    callback = function ()
    vim.cmd('setlocal commentstring=//%s')
    end,
    group    = generalSettingsGroup,
})

vim.filetype.add({
  extension = {
    jai = 'jai'
  }  
})
vim.api.nvim_create_autocmd('FileType', {
    pattern = { 'jai' },
    callback = function ()
        vim.cmd('setlocal commentstring=//%s')
    end,
    group = generalSettingsGroup,
})

require'nvim-treesitter.configs'.setup {
    ensure_installed = { 'c', 'lua', 'vim', 'vimdoc', 'query', 'rust', 'zig', 'cpp', 'latex', 'python'},
    sync_install     = false,
    auto_install     = false,
    ignore_install   = {},

    highlight        = {
        enable  = true,
        disable = {'jai', 'markdown'},
        additional_vim_regex_highlighting = false,
    },
}
local parser_config = require 'nvim-treesitter.parsers'.get_parser_configs()
parser_config.jai = {
    install_info = {
        url   = '~/tmp/jai-compiler/tree-sitter-jai', -- local path or git repo
        files = {'src/parser.c', 'src/scanner.cc'},   -- note that some parsers also require src/scanner.c or src/scanner.cc
        generate_requires_npm          = false,       -- if stand-alone parser without npm dependencies
        requires_generate_from_grammar = true,        -- if folder contains pre-generated src/parser.c
    },
    filetype = 'jai', -- if filetype does not match the parser name
}

-- Let's configure undotree plugin
vim.g.undotree_HelpLine           = 0
vim.g.undotree_SetFocusWhenToggle = 1
vim.g.undotree_SplitWidth         = 40
vim.g.undotree_DiffpanelHeight    = 15
vim.g.undotree_DiffAutoOpen       = 1
vim.g.undotree_ShortIndicators    = 1


require('nvim-surround').setup({
    keymaps = {
        insert          = '<C-g>s',
        insert_line     = '<C-g>S',
        normal          = 'gs',
        normal_cur      = 'gss',
        normal_line     = 'gS',
        normal_cur_line = 'gSS',
        visual          = 'gs',
        visual_line     = 'gS',
        delete          = 'ds',
        change          = 'cs',
    },
})

-- configure leap mappings
require('leap').add_default_mappings()
vim.keymap.set({'n', 'v', 'x', 'o'}, 's', function ()
    local current_window = vim.fn.win_getid()
    require('leap').leap { target_windows = { current_window } }
end)

vim.cmd('syntax sync minlines=10000')

--- Bookmark config
vim.g.bookmark_save_per_working_dir    = 1
vim.g.bookmark_auto_save               = 0
vim.g.bookmark_no_default_key_mappings = 1
vim.g.bookmark_display_annotation      = 1
vim.keymap.set('n', 'mm', '<Plug>BookmarkToggle', {})
vim.keymap.set('n', 'mi', '<Plug>BookmarkAnnotate', {})
vim.keymap.set('n', 'mc', '<Plug>BookmarkClear', {})
vim.keymap.set('n', 'mj', '<Plug>BookmarkNext', {})

telescope.load_extension('vim_bookmarks')
local bookmark_actions = telescope.extensions.vim_bookmarks.actions
vim.keymap.set('n', 'ml',
    function ()
        telescope.extensions.vim_bookmarks.all {
            width_text=80,
            width_line=4,
            attach_mappings = function(_, map)  -- @warning this doesnt work right now
                map('i', '<C-d>', bookmark_actions.delete_selected_or_at_cursor)
                return true
            end
        }
    end,
{})

vim.keymap.set('n', 'm<A-l>', function()  telescope.extensions.vim_bookmarks.current_file({only_annotated=true}) end, {})
vim.keymap.set('n', 'mL', telescope.extensions.vim_bookmarks.current_file, {})

--- Neoclip config
require('neoclip').setup({
    keys = {
        telescope = {
            i = {
                select       = '<cr>',
                paste        = '<A-p>',
                paste_behind = '<A-P>',
                replay       = '<A-q>',  -- replay a macro
                delete       = '<A-d>',  -- delete an entry
                edit         = '<A-e>',  -- edit an entry
                custom       = {},
            },
            n = {
                select       = '<cr>',
                paste        = 'p',
                --- It is possible to map to more than one key.
                -- paste     = { 'p', '<c-p>' },
                paste_behind = 'P',
                replay       = 'q',
                delete       = 'd',
                edit         = 'e',
                custom       = {},
            },
        },
    }
})
telescope.load_extension('neoclip')
vim.keymap.set({'n', 'i', 'v', 'x'}, '<C-p>', telescope.extensions.neoclip.neoclip, {})

require('bufdel').setup {
  next = 'alternate',
  quit = false,  -- quit Neovim when last buffer is closed
}
vim.keymap.set('n', '<A-q>', require('bufdel').delete_buffer_expr)
vim.keymap.set({'n', 'v'}, '<Leader>q', ':bdelete<CR>')

vim.keymap.set({'n', 'v'}, '<Leader>o', ':only<CR>')

require('zen-mode').setup({
    window = {
        backdrop = 1,
        width    = 100,
        height   = 40,
        options  = {
            number         = false,
            relativenumber = false,
        },
    }
})

vim.api.nvim_create_user_command('Sav', 'mksession! sav.vim', {})
vim.api.nvim_create_user_command('Lod', 'so sav.vim', {})

vim.keymap.set('t', '<A-h>', '<C-\\><C-o><C-w>h', {})
vim.keymap.set('t', '<A-j>', '<C-\\><C-o><C-w>j', {})
vim.keymap.set('t', '<A-k>', '<C-\\><C-o><C-w>k', {})
vim.keymap.set('t', '<A-l>', '<C-\\><C-o><C-w>l', {})
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>', {})
vim.keymap.set('t', '<C-O>', '<C-\\><C-o>', {})


-- oil
require("oil").setup()
vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" })

-- lualine
require('status_line')

-- this is a custom module in the lua folder!
require('lsp_config')

-- luasnip
-- See https://www.ejmastnak.com/tutorials/vim-latex/luasnip/

require("luasnip").config.set_config({ -- Setting LuaSnip config
  enable_autosnippets = true,
  store_selection_keys = "<Tab>",
})

-- Yes, we're just executing a bunch of Vimscript, but this is the officially
-- endorsed method; see https://github.com/L3MON4D3/LuaSnip#keymaps
vim.cmd[[
" Use Tab to expand and jump through snippets
imap <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>' 
smap <silent><expr> <Tab> luasnip#jumpable(1) ? '<Plug>luasnip-jump-next' : '<Tab>'

" Use Shift-Tab to jump backwards through snippets
imap <silent><expr> <S-Tab> luasnip#jumpable(-1) ? '<Plug>luasnip-jump-prev' : '<S-Tab>'
smap <silent><expr> <S-Tab> luasnip#jumpable(-1) ? '<Plug>luasnip-jump-prev' : '<S-Tab>'
]]

require("luasnip.loaders.from_lua").lazy_load({paths = "~/.config/nvim/snips/"})

-- To open new buffers on last line 
vim.api.nvim_create_autocmd("BufReadPost", {
    callback = function()
        local mark = vim.api.nvim_buf_get_mark(0, '"')
        if mark[1] > 1 and mark[1] <= vim.api.nvim_buf_line_count(0) then
            vim.api.nvim_win_set_cursor(0, mark)
        end
    end,
})


