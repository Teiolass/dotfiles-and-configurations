-- Abbreviations used in this article and the LuaSnip docs
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local d = ls.dynamic_node
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local rep = require("luasnip.extras").rep

return {

s({trig="ff", dscr="Expands 'ff' into '\\frac{}{}'"},
  {
    t("\\frac{"),
    i(1),
    t("}{"),
    i(2),
    t("}")
  }
),

s({trig="eq", dscr="A LaTeX equation environment"},
  fmt(
    [[
      \begin{equation*}
          <>
      \end{equation*}
    ]],
    { i(1) },
    { delimiters = "<>" }
  )
),

s({trig="hat", dscr="A LaTeX equation environment"},
  fmt(
    [[\hat{<>}]],
    { i(1) },
    { delimiters = "<>" }
  )
),

s({trig="env", snippetType="snippet"},
  fmta(
    [[
      \begin{<>}
      <>
      \end{<>}
    ]],
    {
      i(1),
      i(2),
      rep(1),
    }
  )
),

s({trig="fig", snippetType="snippet"},
  fmta(
    [[
    \begin{figure}[ht] 
    \makebox[\textwidth][c]{\includegraphics{<>}}
    \caption{<>} 
    \label{fig:<>} 
    \end{figure}
    ]],
    {
      i(1), i(2), i(3)
    }
  )
),
}

