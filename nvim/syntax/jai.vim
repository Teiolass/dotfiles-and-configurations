if exists("b:current_syntax")
  finish
endif

syntax sync fromstart

syntax keyword jaiUsing using nextgroup=jaiUsingSpec
syntax keyword jaiNew new
syntax keyword jaiDelete delete
syntax keyword jaiCast cast nextgroup=jaiCastSpec

syntax keyword jaiStruct struct union
syntax keyword jaiEnum enum enum_flags
syntax keyword jaiOperatorOverload operator

syntax keyword jaiIf if ifx
syntax keyword jaiThen then
syntax keyword jaiCase case
syntax keyword jaiElse else
syntax keyword jaiFor for
syntax keyword jaiWhile while

syntax keyword jaiBreak break
syntax keyword jaiContinue continue

syntax keyword jaiDataType void string int float float32 float64 u8 u16 u32 u64 s8 s16 s32 s64 bool f32 f64
syntax keyword jaiBool true false
syntax keyword jaiNull null
setlocal iskeyword+=---
syntax keyword jaiUndefined ---

syntax keyword jaiReturn return
syntax keyword jaiDefer defer

syntax keyword jaiInline inline
syntax keyword jaiNoInline no_inline

syntax keyword jaiGenericKeyword code_of

syntax keyword jaiIt it it_index
syntax keyword jaiContext context
syntax keyword jaiInterface interface

syntax region jaiString start=/\v"/ skip=/\v\\./ end=/\v"/

syntax keyword jaiAutoCast xx
syntax keyword jaiPushContext push_context

syntax match jaiTagNote "@\<\w\+\>" display


syntax match jaiInteger "\v[^a-zA-Z0-9_]@<=[0-9][0-9_]*" display
syntax match jaiFloat "\v[^a-zA-Z]@<=[0-9][0-9_]*((\.[0-9][0-9_]*)([eE][+-]?[0-9_]+)|(\.[0-9][0-9_]*)|([eE][+-][0-9_]+))" display
syntax match jaiHex "\v[^a-zA-Z]@<=0x[0-9A-Fa-f_]+" display

syntax match jaiClass "\v<[A-Z]\w+>" display
syntax match jaiConstant "\v[^a-z]@<=[A-Z][A-Z0-9_]*>" display
syntax match jaiConstantEnum "\v([^a-zA-Z0-9_\.\]\)]\.)@<=[A-Za-z_][A-Za-z0-9_]*" display

syntax match jaiVariableDeclaration "\v(^(\s*using)?)@<=\s*\zs((<\w+>)|,\s)+(\s*:\=)@="
syntax match jaiVariableDeclaration2 "\v(^(\s*using)?)@<=\s*\zs((<\w+>)|,\s)+(\s*: [\w\(\)\s\*\[\]\.]*)@="
syntax match jaiConstantDeclaration "\v(^(\s*using)?)@<=\s*\zs((<\w+>)|,\s)+(\s*:: .*[;(#string)].*$)@="

syntax match jaiStructDefn "\v\w+(\s*::\s*struct)@=" display
syntax match jaiEnumDefn "\v\w+(\s*::\s*enum)@=" display

syntax match jaiFunctionDefinition "\v<\w*>(\s*::(\s|inline|#)*\([^;]*$)@="
" syntax match jaiDynamicFunction "\v<\w*(\s*:\=\s*\(.*\))@="

" syntax match jaiMacro "#\<\w\+\>" display
syntax match jaiMacro "\v#<\w+>(,<\w+>)?" display

syntax match jaiTemplate "\v\$?\$<\w+>"

syntax match jaiOperator "/"
syntax match jaiOperator "->"
syntax match jaiOperator "::"
syntax match jaiOperator ":"
syntax match jaiOperator ":="
syntax match jaiOperator "="
syntax match jaiOperator "=="
syntax match jaiOperator ">"
syntax match jaiOperator "<"
syntax match jaiOperator ">="
syntax match jaiOperator "<="
syntax match jaiOperator "+"
" syntax match jaiOperator "\v\-[^0-9]"
syntax match jaiOperator "-"
syntax match jaiOperator "*"
syntax match jaiOperator "+="
syntax match jaiOperator "-="
syntax match jaiOperator "*="
syntax match jaiOperator "\~"
syntax match jaiOperator "\~="
syntax match jaiOperator "/="
syntax match jaiOperator "%"
syntax match jaiOperator "%="
syntax match jaiOperator ">>"
syntax match jaiOperator "<<"
syntax match jaiOperator ">>="
syntax match jaiOperator "<<="
syntax match jaiOperator "&&"
syntax match jaiOperator "||"
syntax match jaiOperator "\^"
syntax match jaiOperator "&="
syntax match jaiOperator "|="
syntax match jaiOperator "\^="
syntax match jaiOperator "!"
syntax match jaiOperator "&"
syntax match jaiOperator "|"

syntax match jaiUsingSpec "\v,(except|only|map)" contained
highlight def link jaiUsingSpec Keyword

syntax match jaiCastSpec "\v,force|,FORCE" contained
highlight def link jaiCastSpec Keyword

syntax match jaiImportMacro "#import" nextgroup=jaiImportSpec
syntax match jaiImportSpec "\v,dir|,file" contained
highlight def link jaiImportSpec Macro
highlight def link jaiImportMacro Macro

syntax match jaiCommentNote "@\<\w\+\>" contained display
syntax match jaiLineComment "//.*" contains=jaiCommentNote
syntax region jaiBlockComment start=/\v\/\*/ end=/\v\*\// contains=jaiBlockComment, jaiCommentNote

syntax match jaiFunction "\v<\w*>(\s*\()@="
syntax match jaiAsmMacro "#asm" nextgroup=jaiAsmHead skipwhite
syntax match jaiAsmHead "\v\s*\w*\s*" nextgroup=jaiAsm skipwhite contained
syntax region jaiAsm start="\v\{" end= "\v\}" contained
highlight def link jaiAsmMacro Macro
highlight def link jaiAsmHead String

syntax match jaiStringMacro "#string" nextgroup=jaiStringBlockShader,jaiStringBlockString,jaiStringBlockCode,jaiStringBlockDone,jaiStringBlockOther skipwhite
syntax match jaiStringBlockOther "\v<\w+>" contained
syntax region jaiStringBlockShader start="SHADER" end="SHADER" contained
syntax region jaiStringBlockDone start="DONE" end="DONE" contained
syntax region jaiStringBlockString start="STRING" end="STRING" contained
syntax region jaiStringBlockCode   start="CODE"   end="CODE"   contained transparent
syntax match jaiStringBlockCodeDelimiter "CODE" contained
highlight def link jaiStringBlockShader String
highlight def link jaiStringBlockString String
highlight def link jaiStringBlockDone   String
" highlight def link jaiStringBlockCode   String
highlight def link jaiStringBlockCodeDelimiter String

highlight def link jaiUsing Keyword
highlight def link jaiNew Keyword
highlight def link jaiCast Keyword
highlight def link jaiAutoCast Keyword
highlight def link jaiDelete Keyword
highlight def link jaiReturn Keyword
highlight def link jaiDefer Keyword
highlight def link jaiOperatorOverload Keyword

highlight def link jaiInline Keyword
highlight def link jaiNoInline Keyword

highlight def link jaiBreak Keyword
highlight def link jaiContinue Keyword

highlight def link jaiString String

highlight def link jaiGenericKeyword Keyword

highlight def link jaiStruct Keyword
highlight def link jaiEnum Keyword

highlight def link jaiFunctionDefinition FunctionDefinition
highlight def link jaiDynamicFunction Function
highlight def link jaiFunction Function

highlight def link jaiMacro Macro
highlight def link jaiStringMacro Macro
highlight def link jaiIf Conditional
highlight def link jaiThen Conditional
highlight def link jaiCase Conditional
highlight def link jaiElse Conditional
highlight def link jaiFor Repeat
highlight def link jaiWhile Repeat

highlight def link jaiLineComment Comment
highlight def link jaiBlockComment Comment
highlight def link jaiCommentNote Todo

highlight def link jaiClass Type

highlight def link jaiTemplate Macro

highlight def link jaiTagNote Macro
highlight def link jaiDataType Type
highlight def link jaiBool Boolean
highlight def link jaiConstant Constant
highlight def link jaiConstantEnum Constant
highlight def link jaiNull Constant
highlight def link jaiUndefined Constant
highlight def link jaiInteger Number
highlight def link jaiFloat Float
highlight def link jaiHex Number

highlight def link jaiOperator Operator
highlight def link jaiVariableDeclaration IdentifierImportant
highlight def link jaiVariableDeclaration2 IdentifierImportant
highlight def link jaiConstantDeclaration Constant

highlight def link jaiIt VariableBuiltin
highlight def link jaiContext VariableBuiltin
highlight def link jaiInterface Macro

highlight def link jaiStructDefn TypeImportant
highlight def link jaiEnumDefn TypeImportant
highlight def link jaiPushContext Macro

let b:current_syntax = "jai"
