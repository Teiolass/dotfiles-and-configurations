from time import sleep
import os


cmd = 'amixer sget Master | grep Left:'
vol = os.popen(cmd).read()
if '[on]' in vol:
    check = True
else:
    check = False
vol = vol.split('[')[1]
vol = vol.split(']')[0]
if check:
    print(' ' + vol)
else:
    print(u'\uf026')
sleep(0.1)
