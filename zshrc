# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000000
SAVEHIST=1000000
setopt autocd extendedglob notify
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install

# the custom one adds C-g
# source ~/.scripts/dir-history.zsh
source /usr/share/fzf/key-bindings.zsh

# without this delete key doesnt work
bindkey "^[[3~" delete-char

fpath=(~/.scripts/zsh_completions/ $fpath)

# The following lines were added by compinstall
zstyle :compinstall filename '/home/teiolass/.zshrc'
autoload -Uz compinit
compinit
# End of lines added by compinstall

autoload -U colors && colors
PROMPT="%{$fg_bold[green]%}%n@%m%{$reset_color%}:%{$fg_bold[blue]%}%~%{$reset_color%}$ "

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS' --color=fg:#ebdbb2,bg:#1d2021,hl:#98971a --color=fg+:#fbf1c7:bold,bg+:#3c3836,gutter:#1d2021,hl+:#b8bb26 --color=info:#bdae93,prompt:#d79921,pointer:#fabd2f --color=marker:#d79921,spinner:#458588,header:#458588'
export FZF_DEFAULT_COMMAND='fd --type file --hidden --no-ignore --no-follow'
export FZF_CTRL_T_COMMAND='fd --type file --hidden --no-ignore --no-follow'
export FZF_ALT_C_COMMAND='fd --type d . --color=never --hidden'
export FZF_CTRL_G_COMMAND='dirs -p'

autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M emacs "\ee" edit-command-line 

bindkey -M emacs "^[[1;5C" forward-word
bindkey -M emacs "^[[1;5D" backward-word

export EDITOR='nvim'
export TERMINAL=kitty
alias ssh='env TERM=xterm-256color ssh' # allows kitty to work with ssh

export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

export LS_COLORS="di=34;1:ex=32:ln=36:or=31:*.pdf=35"
export EXA_COLORS="da=37:sn=32:sb=32;1"
export EXA_ICON_SPACING=2 
export FPATH="$HOME/.config/eza/completions/zsh:$FPATH"
alias ls='echo "you should use eza instead" && ls --color=always'
alias l='eza --no-user --no-permissions --header --created --modified --group-directories-first'
alias ll='eza --long --no-user --no-permissions --header --icons --created --modified --group-directories-first'
alias la='ll --all'
alias l1='ll --tree --level=2'
alias lg='ll --git'

alias rmf='rm -rf'
alias rip='rip --graveyard /home/teiolass/Tear/Graveyard'

alias neww='pwd | kitty &'
alias new='pwd | kitty & disown'
alias rr='ranger'
alias ff='fzf --multi'
alias clip='xsel -i -b'
alias less='less -r'
alias bat='bat --theme gruvbox-dark'
alias kk='zk --notebook-dir=$HOME/Documents/notebook/ --working-dir=$HOME/Documents/notebook/'

alias cd='z'
alias doc='cd ~/Documents/'
alias dow='cd ~/Downloads/'
alias lb='cd ~/Documents/library/'
alias tmp='cd ~/tmp/'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'



alias zt='__zathura_disown'
alias ztc='__zathura_and_exit'
__zathura_disown() {
    if [[ -n "$@" ]] then
        nohup zathura "$@" >/dev/null 2>/dev/null &
        echo "opening  '$@'"
        return 0
    else
        local buf="$(FZF_CTRL_T_COMMAND='fd --type file --no-ignore -e pdf -e epub -e djvu -e ps' __fsel)"
        local ret=$?
        if [[ -n $buf ]] then
            buf=${(MS)buf##[[:graph:]]*[[:graph:]]}
            buf="$(eval printf '%s\\n' "$buf")"
            echo "opening  '$buf'"
            nohup zathura "$buf" >/dev/null 2>/dev/null &
            return 0
        else
            return 1
        fi
    fi
}
__zathura_and_exit () {
    __zathura_disown $@
    local ret=$?
    if [[ $ret -eq 0 ]] then
        kill $PPID
    fi
}
__zathura_mod_autocomp () {
    compdef '_files -g "*.pdf *.ps *.djvu *.epub"' zt ztc __zathura_disown __zathura_and_exit
}
compdef __zathura_mod_autocomp __zathura_disown
compdef __zathura_mod_autocomp __zathura_and_exit


vim() {
    if [[ -a sav.vim ]] && [[ $# -eq 0 ]] then
        nvim -S sav.vim
    else
        nvim $@
    fi
}

autoload -U select-word-style
select-word-style bash

eval "$(zoxide init zsh)"
eval "$(starship init zsh)"
